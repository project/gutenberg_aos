(function(wp) {

  var addFilter = wp.hooks.addFilter;
  var createHigherOrderComponent = wp.compose.createHigherOrderComponent;
  var __ = wp.i18n.__;
  var createElement = wp.element.createElement;
  var Fragment = wp.element.Fragment;
  var InspectorControls = wp.blockEditor.InspectorControls;
  var PanelBody = wp.components.PanelBody;
  var TextControl = wp.components.TextControl;
  var ToggleControl = wp.components.ToggleControl;
  var SelectControl = wp.components.SelectControl;

  function addAosAnimationAttribute(settings) {
    // Extend attributes to include `aosAnimation` and related properties
    settings.attributes = Object.assign(settings.attributes, {
      aosAnimation: { type: 'string', default: '' },
      aosOffset: { type: 'string', default: '' },
      aosDelay: { type: 'string', default: '' },
      aosDuration: { type: 'string', default: '' },
      aosEasing: { type: 'string', default: '' },
      aosMirror: { type: 'boolean', default: false },
      aosOnce: { type: 'boolean', default: false },
      aosAnchorPlacement: { type: 'string', default: '' },
    });

    return settings;
  }

  addFilter(
    'blocks.registerBlockType',
    'gutenberg-aos/add-aos-animation-attribute',
    addAosAnimationAttribute
  );

  var withInspectorControl = createHigherOrderComponent((BlockEdit) => {
    return (props) => {
      // Inject control into block inspector
      return createElement(
        Fragment,
        {},
        createElement(BlockEdit, props),
        createElement(
          InspectorControls,
          null,
          createElement(
            PanelBody,
            {
              title: __('AOS Animation', 'gutenberg-aos'),
              initialOpen: false,
            },
            // Repeat this structure for each control, adjusted for different attributes
            createElement(SelectControl, {
              label: __('Select AOS Animation', 'gutenberg-aos'),
              value: props.attributes.aosAnimation,
              options: [
                { label: 'None', value: '' },
                { label: 'Fade', value: 'fade' },
                { label: 'Fade Up', value: 'fade-up' },
                { label: 'Fade Down', value: 'fade-down' },
                { label: 'Fade Left', value: 'fade-left' },
                { label: 'Fade Right', value: 'fade-right' },
                { label: 'Fade Up Right', value: 'fade-up-right' },
                { label: 'Fade Up Left', value: 'fade-up-left' },
                { label: 'Fade Down Right', value: 'fade-down-right' },
                { label: 'Fade Down Left', value: 'fade-down-left' },
                { label: 'Flip Up', value: 'flip-up' },
                { label: 'Flip Down', value: 'flip-down' },
                { label: 'Flip Left', value: 'flip-left' },
                { label: 'Flip Right', value: 'flip-right' },
                { label: 'Slide Up', value: 'slide-up' },
                { label: 'Slide Down', value: 'slide-down' },
                { label: 'Slide Left', value: 'slide-left' },
                { label: 'Slide Right', value: 'slide-right' },
                { label: 'Zoom In', value: 'zoom-in' },
                { label: 'Zoom In Up', value: 'zoom-in-up' },
                { label: 'Zoom In Down', value: 'zoom-in-down' },
                { label: 'Zoom In Left', value: 'zoom-in-left' },
                { label: 'Zoom In Right', value: 'zoom-in-right' },
                { label: 'Zoom Out', value: 'zoom-out' },
                { label: 'Zoom Out Up', value: 'zoom-out-up' },
                { label: 'Zoom Out Down', value: 'zoom-out-down' },
                { label: 'Zoom Out Left', value: 'zoom-out-left' },
                { label: 'Zoom Out Right', value: 'zoom-out-right' }
              ],
              onChange: (selectedAnimation) => {
                props.setAttributes({ aosAnimation: selectedAnimation });
              },
            }),
            createElement(wp.components.TextControl, {
              label: __('AOS Offset (pixels)', 'gutenberg-aos'),
              value: props.attributes.aosOffset,
              onChange: (value) => {
                props.setAttributes({ aosOffset: value });
              },
              type: 'number',
              min: 0,
              step: 1
            }),
            createElement(TextControl, {
              label: __('AOS Delay (Milliseconds)', 'gutenberg-aos'),
              value: props.attributes.aosDelay,
              onChange: (value) => {
                props.setAttributes({ aosDelay: value });
              },
              type: 'number',
              min: 0,
              step: 1
            }),
            createElement(TextControl, {
              label: __('AOS Duration (Milliseconds)', 'gutenberg-aos'),
              value: props.attributes.aosDuration,
              onChange: (value) => {
                props.setAttributes({ aosDuration: value });
              },
              type: 'number',
              min: 0,
              step: 1
            }),
            createElement(SelectControl, {
              label: __('AOS Easing', 'gutenberg-aos'),
              value: props.attributes.aosEasing,
              options: [
                { label: 'None', value: '' },
                { label: 'Linear', value: 'linear' },
                { label: 'Ease', value: 'ease' },
                { label: 'Ease-in', value: 'ease-in' },
                { label: 'Ease-out', value: 'ease-out' },
                { label: 'Ease-in-out', value: 'ease-in-out' },
                { label: 'Ease-in-back', value: 'ease-in-back' },
                { label: 'Ease-out-back', value: 'ease-out-back' },
                { label: 'Ease-in-out-back', value: 'ease-in-out-back' },
                { label: 'Ease-in-sine', value: 'ease-in-sine' },
                { label: 'Ease-out-sine', value: 'ease-out-sine' },
                { label: 'Ease-in-out-sine', value: 'ease-in-out-sine' },
                { label: 'Ease-in-quad', value: 'ease-in-quad' },
                { label: 'Ease-out-quad', value: 'ease-out-quad' },
                { label: 'Ease-in-out-quad', value: 'ease-in-out-quad' },
                { label: 'Ease-in-cubic', value: 'ease-in-cubic' },
                { label: 'Ease-out-cubic', value: 'ease-out-cubic' },
                { label: 'Ease-in-out-cubic', value: 'ease-in-out-cubic' },
                { label: 'Ease-in-quart', value: 'ease-in-quart' },
                { label: 'Ease-out-quart', value: 'ease-out-quart' },
                { label: 'Ease-in-out-quart', value: 'ease-in-out-quart' }
              ],
              onChange: (selectedEasing) => {
                props.setAttributes({ aosEasing: selectedEasing });
              },
            }),
            createElement(ToggleControl, {
              label: __('AOS Mirror', 'gutenberg-aos'),
              checked: props.attributes.aosMirror,
              onChange: () => {
                props.setAttributes({ aosMirror: !props.attributes.aosMirror });
              },
            }),
            createElement(ToggleControl, {
              label: __('AOS Once', 'gutenberg-aos'),
              checked: props.attributes.aosOnce,
              onChange: () => {
                props.setAttributes({ aosOnce: !props.attributes.aosOnce });
              },
            }),
            createElement(wp.components.SelectControl, {
              label: __('AOS Anchor Placement', 'gutenberg-aos'),
              value: props.attributes.aosAnchorPlacement,
              options: [
                { label: 'None', value: '' },
                { label: 'Top-Bottom', value: 'top-bottom' },
                { label: 'Top-Center', value: 'top-center' },
                { label: 'Top-Top', value: 'top-top' },
                { label: 'Center-Bottom', value: 'center-bottom' },
                { label: 'Center-Center', value: 'center-center' },
                { label: 'Center-Top', value: 'center-top' },
                { label: 'Bottom-Bottom', value: 'bottom-bottom' },
                { label: 'Bottom-Center', value: 'bottom-center' },
                { label: 'Bottom-Top', value: 'bottom-top' },
              ],
              onChange: (selectedPlacement) => {
                props.setAttributes({ aosAnchorPlacement: selectedPlacement });
              },
            })
            // Additional controls follow...
          )
        )
      );
    };
  }, 'withInspectorControl');

  // Implementation for adding data-aos-* attributes
  addFilter(
    'blocks.getSaveContent.extraProps',
    'gutenberg-aos/add-aos-attributes',
    function(extraProps, blockType, attributes) {

      if (attributes.aosAnimation) {
        extraProps['data-aos'] = attributes.aosAnimation;
        if (attributes.aosOffset) {
          extraProps['data-aos-offset'] = attributes.aosOffset;
        }
        if (attributes.aosDelay) {
          extraProps['data-aos-delay'] = attributes.aosDelay;
        }
        if (attributes.aosDuration) {
          extraProps['data-aos-duration'] = attributes.aosDuration;
        }
        if (attributes.aosEasing) {
          extraProps['data-aos-easing'] = attributes.aosEasing;
        }
        if (attributes.aosMirror && attributes.aosMirror === true) {
          extraProps['data-aos-mirror'] = '';
        }
        if (attributes.aosOnce && attributes.aosOnce === true) {
          extraProps['data-aos-once'] = '';
        }
        if (attributes.aosAnchorPlacement) {
          extraProps['data-aos-anchor-placement'] = attributes.aosAnchorPlacement;
        }
      }
      return extraProps;
    }
  );

  addFilter(
    'editor.BlockEdit',
    'gutenberg-aos/with-inspector-control',
    withInspectorControl
  );

})(window.wp);
