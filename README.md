### Introduction

The Gutenberg AOS module integrates the AOS module (Animate On Scroll) with Drupal's Gutenberg editor, enabling site builders to apply animation effects to Gutenberg blocks. This module facilitates the application of AOS animations without manual code alterations, streamlining development workflows for adding dynamic content visibility.

### Features

Gutenberg AOS extends Drupal Gutenberg blocks by:

- Allowing the assignment of AOS animations via block settings.
- Providing configurable options for animation type, offset, delay, duration, easing, and other AOS parameters.
- Supporting boolean attributes like aos-mirror and aos-once for advanced animation control.

### Configurations

There is no specific configurations for this module as it extends Gutenberg block settings.

- Navigate to the Gutenberg editor.
- Select a block and access its settings.
- Configure AOS animations under the newly added AOS settings section.

For detailed configurations, refer to the [AOS library documentation](https://github.com/michalsnik/aos).

### Additional Requirements

Besides Drupal core, this module requires:

- The Drupal [Gutenberg](https://www.drupal.org/project/gutenberg) module.
- The Drupal [AOS](https://www.drupal.org/project/aos) module.

### Similar projects

Currently, no direct Drupal module integrates Gutenberg with AOS. Similar projects may offer block animations but lack Gutenberg editor integration.
